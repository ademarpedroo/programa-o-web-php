<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One&display=swap" rel="stylesheet">
    <link href="../exercicio_2/style.css" rel="stylesheet">
</head>
<body style="background: #F9F9F9">
<div class="container pt-5" style="width: 550px">
    <div class="shadow" style="background:#f2f1f0; border: 20px solid #f7f6f5">
  <div class="card">

  <h2 id="main" class="p-3 text-center">Desistir nunca foi uma opção</h2>

  <img src="../imagens/tech.jpg" class="card-img-top" alt="...">

  <div class="card-body">

      
      <div class="btn-group" role="group" aria-label="Basic example">
          <a href="exercicio1.php" class="btn btn-primary">Atividade 1 </a>
          <a href="projeto2.php" class="btn btn-primary">Atividade 2 </a>
          <a href="#" class="btn btn-primary">Atividade 3 </a>
          <a href="#" class="btn btn-primary">Atividade 4 </a>
        </div>
    </div>
     
    </div>
  </div>
</div>



</div>

<!-- <script>
    function mostraEmail(){

        var pega_email = document.getElementById("email1");
        var alerta_email = document.getElementById("alerta_email");
        
        if(pega_email.value.length == 0){
                  alerta_email.innerHTML = "Email é obrigatório";
               }
               if(!pega_email.value.length == 0){
                alerta_email.innerHTML = "Digite o email novamente abaixo";
               }
            }

            function alertaUser(){
                var pega_email = document.getElementById("email1");
                var pega_confirm = document.getElementById("email2");

                if(pega_confirm.value == pega_email.value){
                    
                } else{
                    alert("Os emails não coincidem");
                }
            


            }

</script> -->
</body>
</html>