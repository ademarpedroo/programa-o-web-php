<?php
$erro = isset($_GET['erro']) ? $_GET['erro'] : 0;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>

    <div class="login-page">
    <div class="form">
        <form class="login-form" action="validacao.php" autocomplete="off" method="POST">
        
        <input type="text" id="nomeusuario" name="user" required placeholder="Nome de usúario"/>
        <input type="password" id="senha1" name="senha1" required placeholder="Senha"/>

        <?php
    if($erro == 1){
      echo '<h5 class="text-danger text-center">Usuário não cadastrado </h5>';
    } elseif ($erro == 2){
      
      echo '<h5 class="text-danger text-center">Usuário ou senha invalida(s) </h5>';

    }
?>
        <button>Login</button>
        <p class="message">Não tem conta? <a href="Criar_Conta.php">Crie sua conta</a></p>
        <p class="message"><a href="Redefinir_Senha.php">Esqueceu a senha?</a></p>

        </form>
    </div>
    </div>

</body>
</html>
